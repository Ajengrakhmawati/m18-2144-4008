    package com.example.android.marveltravel;

    import android.app.DatePickerDialog;
    import android.app.TimePickerDialog;
    import android.content.DialogInterface;
    import android.content.Intent;
    import android.support.v7.app.AlertDialog;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.util.Log;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.widget.Button;
    import android.widget.CompoundButton;
    import android.widget.DatePicker;
    import android.widget.EditText;
    import android.widget.Spinner;
    import android.widget.Switch;
    import android.widget.TextView;
    import android.widget.TimePicker;
    import android.widget.Toast;

    import java.util.Calendar;

    public class Main2Activity extends AppCompatActivity {

        private Switch mPP;
        private TextView mKepulangan;
        private TextView mTglKepulangan;
        private TextView mWktKepulangan;
        private TextView mPrice;
        private TextView mIsiSaldo;
        int saldoLama;
        private TextView mTopUp;
        EditText isi;
        private TextView mPilihTanggalB;
        private TextView mPilihWaktuB;
        DatePickerDialog datePickerDialog;
        private Button mBtnTiket;
        private Spinner mSpinner;
        private EditText mJumlahTiket;
        private Switch pp;
        private TextView mUcapan;
        int saldoSementara = 0, hargaTiket, jumlahTiket, totalTransaksi;
        String tanggalBerangkat, tanggalPulang, strJumlahTiket, strHargaTotal, tujuan, tiketSpinner, jamBerangkat, jamKepulangan;

        private static final String LOG_TAG = Main2Activity.class.getSimpleName();

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main2);

            mPP = findViewById(R.id.pp);
            mKepulangan = findViewById(R.id.kepulangan);
            mTglKepulangan = findViewById(R.id.tgl_kepulangan);
            mWktKepulangan = findViewById(R.id.wkt_kepulangan);
            mPrice = findViewById(R.id.price);
            mIsiSaldo = findViewById(R.id.isiSaldo);
            mTopUp = findViewById(R.id.topup);
            mPilihTanggalB = findViewById(R.id.tgl_keberangkatan);
            mPilihWaktuB = findViewById(R.id.wkt_keberangkatan);
            mBtnTiket = findViewById(R.id.cek);
            mSpinner = findViewById(R.id.spinner_tujuan);
            mUcapan = findViewById(R.id.ucapan);
            mJumlahTiket = findViewById(R.id.hint_jumlah);
            pp = findViewById(R.id.pp);


            mTopUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showInputDialog();
                }
            });

            mKepulangan.setVisibility(View.GONE);
            mTglKepulangan.setVisibility(View.GONE);
            mWktKepulangan.setVisibility(View.GONE);

             mPP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                 @Override
                 public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                     if (isChecked){
                         mKepulangan.setVisibility(View.VISIBLE);
                         mTglKepulangan.setVisibility(View.VISIBLE);
                         mWktKepulangan.setVisibility(View.VISIBLE);
                     }else{
                         mKepulangan.setVisibility(View.GONE);
                         mTglKepulangan.setVisibility(View.GONE);
                         mWktKepulangan.setVisibility(View.GONE);
                     }
                 }
             });
            mPilihTanggalB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH); // current month
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                    // date picker dialog
                    datePickerDialog = new DatePickerDialog(Main2Activity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    mPilihTanggalB.setText(
                                            dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            });

            mPilihWaktuB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar mCurrentTime = Calendar.getInstance();
                    int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mCurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimerPicker;
                    mTimerPicker = new TimePickerDialog(Main2Activity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            mPilihWaktuB.setText(selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimerPicker.setTitle("Pilih Waktu");
                    mTimerPicker.show();
                }
            });

            mTglKepulangan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH); // current month
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                    // date picker dialog
                    datePickerDialog = new DatePickerDialog(Main2Activity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    // set day of month , month and year value in the edit text
                                    mTglKepulangan.setText(dayOfMonth + "/"
                                            + (monthOfYear + 1) + "/" + year);
                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            });

            mWktKepulangan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(Main2Activity.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            mWktKepulangan.setText(selectedHour + ":" + selectedMinute);
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Pilih Waktu");
                    mTimePicker.show();
                }
            });

            mBtnTiket.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tiketSpinner = mSpinner.getSelectedItem().toString();
                    tanggalBerangkat = mPilihTanggalB.getText().toString();
                    jamBerangkat = mPilihWaktuB.getText().toString();
                    jamKepulangan = mWktKepulangan.getText().toString();
                    tanggalPulang = mTglKepulangan.getText().toString();
                    strJumlahTiket = mJumlahTiket.getText().toString();
                    hargaTiket = Integer.parseInt(tiketSpinner.replaceAll("[\\D]", ""));
                    tujuan = mSpinner.getSelectedItem().toString();

                    if (mPP.isChecked()){
                        hargaTiket = hargaTiket*2;
                    }
                    Log.d("cek","2");
                    jumlahTiket = Integer.parseInt(mJumlahTiket.getText().toString());
                    totalTransaksi = hargaTiket * jumlahTiket;
                    strHargaTotal = Integer.toString(totalTransaksi);
                    Log.d("cek","3");

                    if (saldoSementara >= totalTransaksi){
                        Toast.makeText(Main2Activity.this, "Transaksi Bisa", Toast.LENGTH_SHORT).show();
                        saldoSementara = saldoSementara - totalTransaksi;

                        if (tanggalBerangkat.equals("Pilih Tanggal")){
                            tanggalBerangkat = "null";
                        }
                        if (tanggalPulang.equals("Pilih Tanggal")){
                            tanggalPulang = "null";
                        }
                        if (mPP.isChecked() == false){
                            tanggalPulang = "null";
                        }
                        Intent a = new Intent(Main2Activity.this, summary.class);
                        a.putExtra("tujuan", tujuan);
                        a.putExtra("tanggalBerangkat", tanggalBerangkat);
                        a.putExtra("tanggalPulang", tanggalPulang);
                        a.putExtra("jumlahTiket", strJumlahTiket);
                        a.putExtra("hargaTotal", strHargaTotal);
                        a.putExtra("pp", pp.isChecked());
                        a.putExtra("jamBerangkat", jamBerangkat);
                        a.putExtra("jamKepulangan", jamKepulangan);
                        startActivity(a);
//                    Munculkan TextView TerimaKasih Telah Bertransaksi
                        mUcapan.setVisibility(View.VISIBLE);
                        mPrice.setText(Integer.toString(saldoSementara));
                    } else {
                        Toast.makeText(Main2Activity.this,
                                "Tidak Bisa Transaksi Saldo Kurang, Silahkan Top Up Dahulu :)",
                                Toast.LENGTH_SHORT).show();
                        mUcapan.setVisibility(View.INVISIBLE);
                    }
                }
            });

            Log.d(LOG_TAG, "-------");
            Log.d(LOG_TAG, "onCreate");
        }

        //  Start Log Activity LifeCycle
        @Override
        protected void onResume() {
            super.onResume();
            Log.d(LOG_TAG, "onResume");
        }

        @Override
        protected void onStop() {
            super.onStop();
            Log.d(LOG_TAG, "onStop");
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            Log.d(LOG_TAG, "onDestroy");
        }
//    End Log Activity LifeCycle

        private void showInputDialog(){
            LayoutInflater layoutInflater = LayoutInflater.from(Main2Activity.this);
            View promptView = layoutInflater.inflate(R.layout.saldo, null );
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Main2Activity.this);
            alertDialogBuilder.setView(promptView);
            alertDialogBuilder.setTitle("Masukkan jumlah saldo");

              isi = promptView.findViewById(R.id.editText);
            //set up dialog
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton("Tambah Saldo", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            String saldoAwal = mPrice.getText().toString();
                            String jumlah = isi.getText().toString(); //.trim()

                            int saldoku = Integer.valueOf(saldoAwal);
                            int intSaldo = Integer.valueOf(jumlah);

                            int totalSaldo = intSaldo + saldoku;
//
//
                            mPrice.setText(String.valueOf(totalSaldo));
                            saldoSementara = totalSaldo;
                            Toast.makeText(getApplicationContext(), "Top up Berhasil", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            android.app.AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }

        @Override
        protected void onStart() {
            super.onStart();
            Log.d(LOG_TAG, "onStart");
        }

        @Override
        protected void onPause() {
            super.onPause();
            Log.d(LOG_TAG, "onPause");
        }

        @Override
        protected void onRestart() {
            super.onRestart();
            Log.d(LOG_TAG, "onRestart");
        }
    }
