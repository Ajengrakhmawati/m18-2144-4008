package com.example.android.marveltravel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class summary extends AppCompatActivity {

    private TextView mIsiTujuan;
    private TextView mIsiTglB;
    private TextView mIsiTglP;
    private TextView mTotal;
    private TextView mJmlhTiket;
    private TextView tgl_plg;
    private TextView isi_tglPlg;
    private Button mBtnKonfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        mIsiTujuan = findViewById(R.id.isi_tujuan);
        mIsiTglB = findViewById(R.id.isi_tglBerangkat);
        mIsiTglP = findViewById(R.id.isi_tglPlg);
        mJmlhTiket = findViewById(R.id.isi_jml);
        mTotal = findViewById(R.id.total);
        mBtnKonfirmasi = findViewById(R.id.konfirm);
        tgl_plg = findViewById(R.id.tgl_plg);
        isi_tglPlg = findViewById(R.id.isi_tglPlg);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String tujuan, tanggalBerangkat, tanggalPulang, jumlahTiket, hargaTotal, jamBerangkat, jamKepulangan;
            Boolean pp;
            tujuan = extras.getString("tujuan");
            tanggalBerangkat = extras.getString("tanggalBerangkat");
            tanggalPulang = extras.getString("tanggalPulang");
            jumlahTiket = extras.getString("jumlahTiket");
            hargaTotal = extras.getString("hargaTotal");
            jamBerangkat = extras.getString("jamBerangkat");
            jamKepulangan= extras.getString("jamKepulangan");
            hargaTotal = extras.getString("hargaTotal");
            pp = extras.getBoolean("pp");
            mIsiTujuan.setText(tujuan);
            mIsiTglB.setText(tanggalBerangkat + ", " + jamBerangkat);
            mIsiTglP.setText(tanggalPulang + ", " + jamKepulangan);
            mJmlhTiket.setText(jumlahTiket);
            mTotal.setText(hargaTotal);

            if(!pp){
                tgl_plg.setVisibility(View.GONE);
                isi_tglPlg.setVisibility(View.GONE);
            }
        }

        mBtnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(summary.this, MainActivity.class);
                finish();
            }
        });


    }

    //    Start Log Activity LifeCycle
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onPause");
    }

//    End Log Activity LifeCycle
}
