package com.example.android.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //deklarasi variabel
    private TextView mHasilTextView;
    private Button mCekButton;
    private EditText mAlas;
    private EditText mTinggi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //cara get ID
        mHasilTextView = findViewById(R.id.hasil);
        mCekButton = findViewById(R.id.cek);
        mAlas = findViewById(R.id.alas);
        mTinggi = findViewById(R.id.tinggi);
    }

    @Override
    public void onClick(View v) {
        String inputAlas = mAlas.getText().toString().trim();
        String inputTinggi = mTinggi.getText().toString().trim();

        Double masukAlas = Double.valueOf(inputAlas);
        Double masukTinggi = Double.valueOf(inputTinggi);

        Double Hasil = masukAlas * masukTinggi;
        mHasilTextView.setText(String.valueOf(Hasil));
    }
}
