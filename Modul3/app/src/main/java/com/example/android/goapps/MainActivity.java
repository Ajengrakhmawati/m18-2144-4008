package com.example.android.goapps;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    Button batal, tambah;
    RecyclerView mRecyclerView;
    Dialog dialog;
    private ArrayList<PolaItem> daftarUser;
    private list userAdapter;
    ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mRecyclerView = findViewById(R.id.recycler_view);
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, gridColumnCount));

        daftarUser = new ArrayList<>();

        if (savedInstanceState != null) {
            daftarUser.clear();

            for (int i = 0; i < savedInstanceState.getStringArrayList("nama").size(); i++) {

                daftarUser.add(new PolaItem(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("job").get(i),
                        savedInstanceState.getIntegerArrayList("gender").get(i)));

            }
        }

        userAdapter = new list(daftarUser, this);
        mRecyclerView.setAdapter(userAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPopup(view);
            }
        });


    ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
            ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            int dari = viewHolder.getAdapterPosition();
            int ke = target.getAdapterPosition();
            Collections.swap(daftarUser, dari, ke);
            userAdapter.notifyItemMoved(dari, ke);

            return true;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            daftarUser.remove(viewHolder.getAdapterPosition());
            userAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());

        }
    });
        helper.attachToRecyclerView(mRecyclerView);
}


    public void ShowPopup(View v) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.activity_add_dialog);
        final EditText mNama,mPekerjaan;
        final Spinner mGender;
        mNama = dialog.findViewById(R.id.editText_masukNama);
        mPekerjaan = dialog.findViewById(R.id.editText_masukPekerjaan);

        Button tambah =dialog.findViewById(R.id.btn_tambah);
        Button batal = dialog.findViewById(R.id.btn_batal);

        mGender = dialog.findViewById(R.id.spinner_jk);

        String[]list={"Male","Female"};

        ArrayAdapter<String>adapterX = new ArrayAdapter(dialog.getContext(),android.R.layout.simple_spinner_item,list);
        mGender.setAdapter(adapterX);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftarUser.add(new PolaItem(mNama.getText().toString(),mPekerjaan.getText().toString(),mGender.getSelectedItemPosition()+1));
                userAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onSaveInstanceState(Bundle state) {
        ArrayList<String> tmpNama = new ArrayList<>();
        ArrayList<String> tmpJob = new ArrayList<>();
        ArrayList<Integer> tmpGender = new ArrayList<>();

        for (int i=0; i<daftarUser.size(); i++){
            tmpNama.add(daftarUser.get(i).getNama());
            tmpJob.add(daftarUser.get(i).getJob());
            tmpGender.add(daftarUser.get(i).getIkon());
        }

        state.putStringArrayList("nama", tmpNama);
        state.putStringArrayList("job", tmpJob);
        state.putIntegerArrayList("gender", tmpGender);

        super.onSaveInstanceState(state);
    }


}
