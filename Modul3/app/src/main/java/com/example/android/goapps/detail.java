package com.example.android.goapps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class detail extends AppCompatActivity {

    TextView detailName, detailJob;
    ImageView foto;
    String mNama, mJob;
    int ikon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        detailName = findViewById(R.id.txt_nama);
        detailJob = findViewById(R.id.txt_job);
        foto = findViewById(R.id.user);

        mNama = getIntent().getStringExtra("nama");
        mJob = getIntent().getStringExtra("job");
        ikon = getIntent().getIntExtra("gender", 2);


        detailName.setText(mNama);
        detailJob.setText(mJob);

        switch (ikon){
            case 1:
                foto.setImageResource(R.drawable.boy);
                break;
            case 2:
            default:
                foto.setImageResource(R.drawable.woman);

        }
    }
}
