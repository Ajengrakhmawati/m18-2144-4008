package com.example.android.goapps;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class list extends RecyclerView.Adapter<list.ViewHolder> {

    private ArrayList<PolaItem> listUser;
    private Context mContext;

    public list(ArrayList<PolaItem> listUser, Context mContext) {
        this.listUser = listUser;
        this.mContext = mContext;
    }



    @NonNull
    @Override
    public list.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.activity_list, viewGroup, false ));
    }

    @Override
    public void onBindViewHolder(@NonNull list.ViewHolder viewHolder, int posisi) {

        PolaItem user = listUser.get(posisi);
        viewHolder.bindTo(user);

    }

    @Override
    public int getItemCount() {
        return listUser.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nama,job;

        private ImageView foto;
        private int ikonCode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.txtName);
            job = itemView.findViewById(R.id.txtJob);
            foto = itemView.findViewById(R.id.imageWoman);

            itemView.setOnClickListener(this);

        }

        public void bindTo(PolaItem user) {
            nama.setText(user.getNama());
            job.setText(user.getJob());

            ikonCode = user.getIkon();
            switch (user.getIkon()){
                case 1:
                    foto.setImageResource(R.drawable.boy);
                    break;
                case 2:
                default:
                    foto.setImageResource(R.drawable.woman);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetailActivity = new Intent(v.getContext(),detail.class);
            toDetailActivity.putExtra("nama",nama.getText().toString());
            toDetailActivity.putExtra("gender",ikonCode);
            toDetailActivity.putExtra("job",job.getText().toString());
            v.getContext().startActivity(toDetailActivity);
        }
    }
}

