# Dokumentasi Modul 3
## Ajeng Rakhmawati 1202162144 SI4008

Kali ini kita belajar mengenai :
- Adapter
- CardView
- Landscape Orientation

### Adapter
Adapter adalah jembatan antara dan AdapterView (contohnya ListView) dengan data. Adapter inilah yang menyediakan akses ke item data dan juga bertanggung jawab untuk membuat sebuah View pada setiap item dalam kumpulan data. Selain itu Adapter juga yang mengelola data model.

### CardView
CardView yang sebetulnya FrameLayout dengan sudut yang membulat dan shadow berdasarkan nilai elevation-nya. Perlu dicatat bahwa sebuah CardView membungkus suatu layout dan biasanya dipakai sebagai container untuk setiap item di dalam ListView atau RecyclerView.

### Landscape Orientation
Landscape orientation yaitu mengubah tampilan potrait menjadi landscape. Nantinya ketika ditampilkan secara landscape tampilan akan tetap menyimpan data.

Berikut hasil dari pengerjaan di modul 3 kali ini

## 1. Tampilan Splash Screen
![Gambar](./splash.jpg)

## 2. Tampilan Utama
![Gambar](./main.jpg)

## 3. Tampilan Add Dialog
![Gambar](./adddialog.jpg)

## 4. Tampilan List
![Gambar](./details2.jpg)

## 5. Tampilan landscape
![Gambar](./landscape.jpg)

## 6. Tampilan Profil
![Gambar](./profil.jpg)

## 7. Tampilan Setelah dihapus
![Gambar](./hapus.jpg)