### Modul 4 teridiri dari beberapa Activity :
- Splash
- Registrasi
- Login
- Input Menu
- MainActivity
- Card
- AdapterCard


### Berikut merupakan hasil dari run program:
1. Splash
<img src="./dokumentasi/splash.jpg"/>

2. Login
<img src="./dokumentasi/login.jpg"/>

3. Registrasi
<img src="./dokumentasi/regis.jpg"/>

4. Input Menu
<img src="./dokumentasi/input.jpg"/>

5. List
<img src="./dokumentasi/list.jpg"/>