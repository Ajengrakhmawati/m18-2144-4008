package com.example.android.eadrestaurant;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class registrasi extends AppCompatActivity {
    EditText nama, email, pass;
    FirebaseAuth mAuth;
    Button btnDaftar;
    TextView mLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        nama = findViewById(R.id.edtNama);
        email = findViewById(R.id.edt_Email);
        pass = findViewById(R.id.edt_Password);
        btnDaftar = findViewById(R.id.btnDaftar);
        mLogin = findViewById(R.id.login);

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String namaku = nama.getText().toString();
                String emailku = email.getText().toString();
                String passku = pass.getText().toString();

                regist(emailku, passku);
                startActivity(new Intent(registrasi.this,Login.class));
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(registrasi.this, Login.class));
            }
        });

    }
    public void regist(String email, String password){
        if (check()){
            mAuth = FirebaseAuth.getInstance();
            mAuth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                FirebaseUser user = mAuth.getCurrentUser();
                                UserProfileChangeRequest userProfileChangeRequest = new UserProfileChangeRequest.Builder().setDisplayName(nama.getText().toString()).build();

                                user.updateProfile(userProfileChangeRequest);
                                Toast.makeText(registrasi.this, "oiiii", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(registrasi.this,Login.class));
                                finish();
                            }else {
                                Log.d("error", "onComplete: "+task.getException());
                            }
                        }
                    });
        }
    }


    public boolean check(){
        if (nama.getText().toString().equals("")){
            nama.setError("Masukkan Namamu");
            nama.requestFocus();
            return false;
        }
        if (email.getText().toString().equals("")){
            email.setError("Masukkan Namamu");
            email.requestFocus();
            return false;
        }

        if (pass.getText().toString().equals("")){
            pass.setError("Masukkan Namamu");
            pass.requestFocus();
            return false;
        }
        return true;
    }


    public void gotoLogin(View view){
        startActivity(new Intent(registrasi.this,Login.class));
        finish();
    }
}





