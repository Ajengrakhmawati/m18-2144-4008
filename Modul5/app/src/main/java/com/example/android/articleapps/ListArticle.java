package com.example.android.articleapps;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.android.articleapps.Database.AdapterArticle;
import com.example.android.articleapps.Database.Artikel;
import com.example.android.articleapps.Database.ArtikelResources;

import java.util.ArrayList;

public class ListArticle extends AppCompatActivity {

    ArtikelResources dbHelper;
    RecyclerView recyclerView;
    ArrayList<Artikel> daftarArtikel;
    AdapterArticle adapterArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setupSharedPreferences();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_article);
        getSupportActionBar().hide();
        dbHelper = new ArtikelResources(getApplicationContext());
        daftarArtikel = new ArrayList<>();
        recyclerView = findViewById(R.id.rv_listArticle);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListArticle.this));
        adapterArticle = new AdapterArticle(daftarArtikel, ListArticle.this);
        recyclerView.setAdapter(adapterArticle);
        init();
    }
    private void setupSharedPreferences() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        toggleTheme(prefs.getBoolean("nightMode", false));

    }

    public void toggleTheme(Boolean bo) {
        if (bo) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }

    }


    public void init() {
        daftarArtikel.clear();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                ArtikelResources.COLUMN_ID,
                ArtikelResources.COLUMN_TITLE,
                ArtikelResources.COLUMN_AUTHOR,
                ArtikelResources.COLUMN_DESC,
                ArtikelResources.COLUMN_DATE
        };

        Cursor cursor = db.query(
                ArtikelResources.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                null               // The sort order
        );



        while (cursor.moveToNext()) {
            Toast.makeText(this, cursor.getString(cursor.getColumnIndex(ArtikelResources.COLUMN_TITLE)), Toast.LENGTH_SHORT).show();
            daftarArtikel.add(new Artikel(
                    cursor.getString(cursor.getColumnIndex(ArtikelResources.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(ArtikelResources.COLUMN_TITLE)),
                    cursor.getString(cursor.getColumnIndex(ArtikelResources.COLUMN_AUTHOR)),
                    cursor.getString(cursor.getColumnIndex(ArtikelResources.COLUMN_DESC)),
                    cursor.getString(cursor.getColumnIndex(ArtikelResources.COLUMN_DATE))
            ));
        }
        cursor.close();
        adapterArticle.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }
}
