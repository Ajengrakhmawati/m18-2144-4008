### Modul 5 (Modul Terakhir)

Berikut merupakan tampilan hasil dari dokumentasi modul 5

1. Splash Screen
<img src="./document/splash_morning.jpg"/>

<img src="./document/splash_night.jpg"/>

2. Halaman menu
<img src="./document/menu_morning.jpg"/>

<img src="./document/menu_night.jpg"/>


3. List Artikel
<img src="./document/list_morning.jpg"/>


4. Create Artikel
<img src="./document/create.jpg"/>


5. Setting
<img src="./document/setting.jpg"/>


6. Detail
<img src="./document/detail_night.jpg"